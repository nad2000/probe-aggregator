
SET client_min_messages = ERROR;
-- UNLISTEN "pa.changes";
-- SELECT pa.push_to_pa();


SELECT count(s.*) FROM pa.stores_ AS s 
WHERE s.id NOT IN (SELECT id FROM pa.stores)


SELECT count(s.*) FROM pa.stores_ AS s LEFT JOIN pa.stores d
USING(id)
WHERE d.id IS NULL

-- 
DELETE FROM pa.stores WHERE (cluster_partition % 2) = 0;
DELETE FROM pa.nodes WHERE version > 4;
VACUUM FULL ANALYZE;

UPDATE pa.stores SET version = version + 1 WHERE (cluster_partition % 3) = 1;
SELECT count(*) FROM pa.stores;
SELECT count(*) FROM pa.nodes;

UPDATE pa.nodes SET version = COALESCE(version + 1, 1) WHERE id IN (SELECT id FROM pa.nodes ORDER BY random() LIMIT 10);

INSERT INTO pa.nodes(ip_addr, host_name)
SELECT ip_addr, host_name||'-'||s::text
FROM generate_series(336,337) AS s(s), pa.nodes AS n;

INSERT INTO pa.stores(
  id, node_id, erfstore_id, probe_name, erfstore_name, 
  tablespace, cluster_partition, vision, last_ts, next_flow_id, 
  archive, created_by, temporary
)
SELECT uuid_generate_v4() AS id, n.id AS node_id, s.erfstore_id, n.host_name, s.erfstore_name||'-'||seq.s::text, 
  s.tablespace, s.cluster_partition, s.vision, s.last_ts, s.next_flow_id, 
  s.archive, s.created_by, s.temporary
FROM erfstores AS s, pa.nodes AS n, generate_series(566,577) AS seq(s);


DELETE FROM pa.nodes;
SELECT * FROM pa.nodes_;
SELECT * FROM pa.nodes;
SELECT * FROM pa.stores_;
SELECT * FROM pa.stores;
SELECT s.*, n.host_name, n.ip_addr FROM pa.stores AS s LEFT JOIN pa.nodes AS n ON n.id = s.node_id;
SELECT * FROM erfstores;
DELETE FROM erfstores WHERE erfstore_name = 'test';

SET client_min_messages = notice;
UNLISTEN "pachanges";
UNLISTEN "pa.changes";
UNLISTEN "pa.changes.web";
NOTIFY "pa.changes.web", 'TEST';

DELETE FROM pa.nodes;
INSERT INTO pa.nodes (host_name, ip_addr, is_pa) values ('box111', '192.168.132.111', true) returning *;
INSERT INTO pa.nodes (host_name, ip_addr, is_pa) values ('box139', '192.168.132.139', true) returning *;
INSERT INTO pa.nodes (host_name, ip_addr) values ('box113', '192.168.132.112') returning *;
INSERT INTO pa.nodes (host_name, ip_addr) values ('box224', '192.168.132.224') returning *;
INSERT INTO pa.nodes (host_name, ip_addr) values ('box225', '192.168.132.225') returning *;
COMMIT;

DELETE FROM pa.nodes WHERE host_name LIKE '%13';


INSERT INTO pa.nodes (host_name, ip_addr, remote) values ('remote_insert', '192.168.132.222', true) returning *;
DELETE FROM pa.nodes WHERE host_name LIKE '%remote%';

INSERT INTO pa.stores (
  node_id,
  id, erfstore_id, probe_name, erfstore_name, cluster_partition, 
  vision, last_ts, next_flow_id, archive, created_by)
SELECT (SELECT id FROM pa.nodes WHERE host_name='box111') AS node_id, 
  id, erfstore_id, probe_name, erfstore_name, cluster_partition, 
  vision, last_ts, next_flow_id, archive, created_by
FROM erfstores;

UPDATE pa.stores SET last_ts=last_ts+1 WHERE id = (SELECT id FROM pa.stores ORDER BY random() LIMIT 1)

-- DELETE FROM pa.nodes;