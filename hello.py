from flask import Flask, Response
import time

request_count = 0
app = Flask(__name__)

@app.route("/")
def hello():
    def resp():
        global request_count
        t0 = time.time()
        yield "Hello there!\n"
        yield "Previous reqest #{0}\n".format(request_count)
        time.sleep(10)
        request_count += 1
        yield "Request #{0}\n".format(request_count)
        yield "Request took {0}\n".format(time.time() - t0)
    return Response(resp(), mimetype="text/plain")
    

if __name__ == "__main__":
    app.run(threaded=True, debug=True)

