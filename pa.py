# -*- coding: utf-8 -*-

import select
#%%

#from flask import Flask, Response, jsonify, request
import time
import sys
import json
import psycopg2.extensions
import psycopg2
import psycopg2.extras
from psycopg2 import connect

from threading import Thread
from multiprocessing import Pool

import socket
import uuid
import random

from cStringIO import StringIO

#%%
source = "192.168.132.111"
#pa_node_ip_addresses = ["192.168.132.111", "192.168.132.139", "192.168.132.115", "192.168.132.144"]
pa_node_ip_addresses = ["192.168.132.111", "192.168.132.139", "192.168.132.115"]
ep_ip_addresses = ["192.168.132.111", "192.168.132.139", "192.168.132.115", "192.168.132.144"]
all_nodes = all_nodes_by_id = all_nodes_by_ip_addr = {}


def is_uuid(value):
    return value is not None and type(value) is uuid.UUID


def host_list_attr_mp(hosts=None):
    """
    Retrieve host attributes in parallel using processes
    """
    if hosts is None:
        hosts = set(pa_node_ip_addresses + ep_ip_addresses)

    pool = Pool(len(hosts))
    res = pool.map(get_node_attr, hosts)
    return res


def host_list_attr(hosts=None):
    """
    Retrieve host attributes in parallel using threads
    """
    if hosts is None:
        hosts = set(pa_node_ip_addresses + ep_ip_addresses)

    class AttrGetter(Thread):
        def __init__(self, host_name):
            Thread.__init__(self)
            self.host_name = host_name
        def run(self):
            self.attr = get_node_attr(self.host_name)

    threads = []
    res = []
    for h in hosts:
        t = AttrGetter(h)
        t.start()
        threads.append(t)
    for t in threads:
        t.join()
        res.append(t.attr)

    return res


def get_all_attr():
    global all_nodes, all_nodes_by_id, all_nodes_by_ip_addr
    if all_nodes is None or all_nodes == {}:
        all_nodes = host_list_attr(set(pa_node_ip_addresses + ep_ip_addresses))
        all_nodes_by_id = dict(zip([n["node_id"] for n in all_nodes], all_nodes))
        all_nodes_by_ip_addr = dict(zip([n["ip_addr"] for n in all_nodes], all_nodes))
    return all_nodes


def get_all_nodes_by_id():
    global all_nodes_by_id
    if not all_nodes_by_id:
        get_all_attr()
    return all_nodes_by_id


def get_all_nodes_by_ip_addr():
    global all_nodes_by_ip_addr
    if not all_nodes_by_ip_addr:
        get_all_attr()
    return all_nodes_by_ip_addr

#%%
def shuffled(a_list, host=None):

    if host is not None:
        return [host]
    else:
        copy_of_the_list = a_list[:]
        random.shuffle(copy_of_the_list)
        return copy_of_the_list


def get_pa_nodes(host=None):
    return get_nodes(host, is_pa=True)


def get_pas_in_cluster(host=None):
    pa_nodes = get_pa_nodes(host)
    pa_nodes = dict(zip([n["ip_addr"] for n in pa_nodes], pa_nodes))

    pas = []
    print pa_nodes
    for i, pa_ip_addr in enumerate(set(pa_node_ip_addresses + pa_nodes.keys())):

        node = pa_nodes.get(pa_ip_addr)
        if node is None:
            node = get_all_nodes_by_ip_addr().get(pa_ip_addr)

        if  host == pa_ip_addr:
            is_alive = is_connected = True
        else:
            try:
                is_connected = get_node_id(host=pa_ip_addr, ip_addr=host) is not None and (pa_ip_addr in pa_nodes)
                is_alive = True
            except Exception as e:
                is_alive = is_connected = False

        if node is None:
            name = "PA%i" % (i+1)
        else:
            name = node.get("host_name")

        pas.append(dict(
            id=node.get("id"),
            ip_addr=pa_ip_addr,
            is_connected=is_connected,
            is_alive=is_alive,
            name=name
        ))
    return pas
    

def get_pas(host=None, probe_ip_addr=None):
    pa_nodes = get_pa_nodes(host)
    pa_nodes = dict(zip([n["ip_addr"] for n in pa_nodes], pa_nodes))

    pas = []
    for i, pa_ip_addr in enumerate(pa_node_ip_addresses):
        node = pa_nodes.get(pa_ip_addr)
        if node is None:
            node = get_all_nodes_by_ip_addr().get(pa_ip_addr)

        is_alive = is_connected = (host == pa_ip_addr)
        if probe_ip_addr is not None and (host != pa_ip_addr):
            try:
                is_connected = get_node_id(ip_addr=probe_ip_addr, host=pa_ip_addr) is not None
                is_alive = True
            except Exception as e:
                is_alive = is_connected = False

        if node is None:
            name = "PA%i" % (i+1)
        else:
            name = node.get("host_name")
        pas.append(dict(
            ip_addr=pa_ip_addr,
            is_connected=is_connected,
            is_alive=is_alive,
            name=name
        ))
    return pas


def get_eps(host=None):
    nodes = get_nodes(host)
    nodes_ip_addrs = [n["ip_addr"] for n in nodes]
    nodes = dict(zip(nodes_ip_addrs, nodes))
    return [dict(
            ip_addr=ip_addr,
            connected=(ip_addr in nodes_ip_addrs),
            id=(nodes[ip_addr]["id"] if ip_addr in nodes else None)) for ip_addr in ep_ip_addresses]


def push_to_pa(ep_addr_ip):
    """
    Places a node INSERT message on the queue
    """
    try:
        db = get_db(host=ep_addr_ip)
        cr = db.cursor()
        cr.execute("SELECT pa.push_to_pa()")
        cr.close()
        db.close()
    except Exception as e:
        print "*** Failed to run pa.push_to_pa()"
        print e


def add_pa_to_cluster(node_id, pa=None):
    node_id = get_node(node_id)
    upsert_node(host=pa, node_id=node_id, is_pa=True)


def remove_node(node_id=None, host=None):
    try:
        db = get_db(host)
        cr = db.cursor()
        cr.execute("DELETE FROM pa.nodes WHERE id=%s", (node_id,))
        cr.close()
        db.close()
    except Exception as e:
        print "*** Failed to delete the node: %s" % node_id
        print e


def get_node_id(ip_addr, host=None):

    for pa in shuffled(pa_node_ip_addresses, host):
        try:
            db = get_db(host=pa)
            cr = db.cursor()
            cr.execute("SELECT id FROM pa.nodes WHERE ip_addr=%s LIMIT 1", (ip_addr,))
            row = cr.fetchone()
            cr.close()
            db.close()
            if row:
                return row[0]
            print "*** Failed to get node ID from %s of %s" % (pa, ip_addr)

        except Exception as e:
            print "*** Failed to get node ID"
            print e
            if host is not None:
                raise e

    print "*** Failed to get node ID"
    return None


def get_node(node_id, host=None):

    pas = shuffled(pa_node_ip_addresses, host)

    try:
        if not is_uuid(node_id):
            node_id = uuid.UUID(node_id)
    except:
        name_or_ip_addr = node_id
        node_id = None

    for pa in pas:
        try:
            db = get_db(host=pa)
            cr = db.cursor()
            if node_id is None:
                cr.execute("SELECT * FROM pa.nodes WHERE host_name=%s OR ip_addr=%s LIMIT 1", (name_or_ip_addr, name_or_ip_addr,))
            else:
                cr.execute("SELECT * FROM pa.nodes WHERE id=%s LIMIT 1", (node_id,))
            row = cr.fetchone()
            cols = [ d[0] for d in cr.description ]
            cr.close()
            db.close()
            if row:
                res = dict(zip(cols, row))
                return res
        except Exception as e:
            print "*** Failed to get node from %s" % pa
            print e

    print "*** Failed to get node ID"

def copy_full(src, dst):

    data = StringIO()

    src_db = get_db(host=src)
    src_cr = src_db.cursor()

    dst_db = get_db(host=dst)
    dst_db.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    dst_cr = dst_db.cursor()

    schema = "pa"
    for table in ["nodes", "stores"]:

        #stage = "{0}_{1}".format(table, random.randint(1,1000000))
        stage = table + "_"
        data.seek(0)

        src_cr.copy_to(data, schema + '.' + table)
        data.seek(0)

        dst_cr.execute("DROP TABLE IF EXISTS {0}.{1}".format(schema, stage))
        sql = "CREATE TABLE {0}.{1} (LIKE {0}.{2})".format(schema, stage, table)
        dst_cr.execute(sql)


        dst_cr.execute("CREATE INDEX {0}_{1}_id_idx ON {0}.{1}(id)".format(schema, stage))
        dst_cr.execute("ANALYZE {0}.{1}".format(schema, stage))

        dst_cr.copy_from(data, table=schema + '.' + stage)

        dst_cr.execute("""
            SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = %s
                AND table_name = %s
        """, (schema, table))
        cols = [c[0] for c in dst_cr.fetchall()]

        sql = ("UPDATE {0}.{2} AS d SET "
            + ",\n  ".join(["{0} = COALESCE(s.{0}, d.{0})".format(c) for c in cols if c != "id"])
            + "\nFROM {0}.{1} AS s\nWHERE s.id = d.id AND s.version > d.version").format(schema, stage, table)
        print "***", sql
        dst_cr.execute(sql)


#        sql = """
#            INSERT INTO {0}.{2}
#            SELECT s.* FROM {0}.{1} AS s
#            WHERE s.id NOT IN (SELECT id FROM {0}.{2})
#        """.format(schema, stage, table)


        sql = """
            INSERT INTO {0}.{2}
            SELECT s.* FROM {0}.{1} AS s LEFT JOIN {0}.{2} d
                USING(id)
            WHERE d.id IS NULL
        """.format(schema, stage, table)
        print "***", sql
        dst_cr.execute(sql)

    src_cr.close()
    src_db.close()

    dst_cr.close()
    dst_db.close()


def clean_data(host):

    db = get_db(host)
    cr = db.cursor()
    cr.execute("DELETE FROM pa.nodes")
    cr.close()
    db.close()

def create_node(pa=None,
                message=None,
                ip_addr=None,
                host_name=None,
                is_pa=False):
    """
    Parameters
    ----------
    pa : str
        PA IP address to attemt to create an entry

    message : dict
        DML message recieved from the trigger

    ip_addr : str
        Node IP adddress

    host_name : str
        Node host name

    is_pa : bool
        "Node is PA" flag
    """

    if pa is None:
        pa = source

    node_id = None

    if message is not None:
        node_id = message.get("id")
        if is_pa is None:
            is_pa = message.get("is_pa")
        version = message.get("version")
        if host_name is None:
            host_name = message.get("host_name")
        if ip_addr is None:
            ip_addr = message.get("ip_addr")

    if ip_addr is None:
        ip_addr = source

    try:
        node_info = get_node_attr(ip_addr)

        if node_info is not None:
            node_id = node_info.get("node_id")
            if host_name is None:
                host_name = node_info.get("host_name")
            if ip_addr is None:
                ip_addr = node_info.get("ip_addr")

        if node_id is not None:
            if not is_uuid(node_id):
                node_id = uuid.UUID(node_id)
        else:
            node_id = uuid.uuid4()

        res = upsert_node(
            node_id=node_id, ip_addr=ip_addr, host_name=host_name,
            is_pa=is_pa, host=pa)

        stores = get_erfstores(host=ip_addr)

        if stores is not None:
            upsert_store(stores, node_id=node_id, host=pa)

        return res

    except Exception as e:
        print "*** Failed to create a node entry at %s: (%s, %s, %s)" % (source, node_id, host_name, ip_addr)
        print e

    print "*** Failed to create node: (%s, %s, %s)" %  (node_id, host_name, ip_addr)


def get_node_attr(ip_addr):
    """
    Retrieve EP info from the node

    Parameters
    ----------
    ip_addr : str
        Probe IP address

    Returns
    -------
    dict
        {node_id, host_name, ip_addr}

    """

    try:
        db = get_db(host=ip_addr)
        cr = db.cursor()
        cr.execute("SELECT * FROM pa.get_node_attr()")
        node_id, host_name, ip_addr = cr.fetchone()
        cr.close()
        db.close()
        return dict(node_id=uuid.UUID(node_id), host_name=host_name, ip_addr=ip_addr)
    except Exception as e:
        print "*** Failed to create a node entry at %s: (%s, %s, %s)" % (source, node_id, host_name, ip_addr)
        print e
        return None


def upsert_node(node_id, ip_addr, host_name, is_pa=False, version=None, host=None):

    if host is None:
        host = source

    if type(node_id) is uuid.UUID:
        node_id = str(node_id)

    try:
        db = get_db(host=host)
        cr = db.cursor()
        cr.execute("""
        -- UPSERT
        WITH s AS (SELECT * FROM pa.nodes WHERE id=%(node_id)s::uuid),
        u AS (UPDATE pa.nodes AS n
            SET
                ip_addr = COALESCE(%(ip_addr)s, s.ip_addr),
                host_name = COALESCE(%(host_name)s, s.host_name),
                is_pa = COALESCE(%(is_pa)s, s.is_pa),
                remote = COALESCE(%(remote)s, s.remote),
                version = COALESCE(%(version)s, s.version+1)
            FROM s
            WHERE n.id = s.id
          RETURNING n.id
        ),
        r(id, ip_addr, host_name, is_pa, remote, "version") -- rows
          AS (VALUES(
                %(node_id)s::uuid,
                %(ip_addr)s::inet,
                %(host_name)s,
                %(is_pa)s,
                %(remote)s,
                %(version)s
            ))
        INSERT INTO pa.nodes (id, ip_addr, host_name, is_pa, remote, "version")
        SELECT r.* FROM r LEFT JOIN u ON u.id = r.id::uuid
        WHERE u.id IS NULL
        """, dict(
            remote=True,
            version=0,
            node_id=node_id,
            ip_addr=ip_addr,
            host_name=host_name,
            is_pa=is_pa))

        cr.close()
        db.close()
    except Exception as e:
        print "*** Failded to UPSERT a node:", (node_id, ip_addr, host_name, is_pa), "into", host
        print e

    return (node_id, ip_addr, host_name, is_pa)


def upsert_store(store, node_id=None, host=None):

    res = None
    if store is not None and type(store) in (list, tuple):
        res = []
        for s in store:
            res.append(upsert_store(s, node_id=node_id, host=host))
        return res

    if host is None:
        host = source

    if node_id is None:
        node_id = store.get("node_id")

    store_id = store.get("id")

    _store = get_store(store_id=store_id, host=host)
    if node_id is None:
        node_id = _store.get("node_id")

    if is_uuid(node_id):
        node_id = str(node_id)


    if "node_id" not in store:
        store["node_id"] = node_id

    try:
        db = get_db(host=host)
        cr = db.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        if _store is None:

            sql = "INSERT INTO pa.stores (" + ", ".join(store) + ") VALUES ("
            sql += ", ".join(["%({0})s".format(k) for k in store]) + ") RETURNING *"

        else:

            sql = "UPDATE pa.stores SET "
            sql += ",\n".join(["{0} = %({0})s".format(k) for k in store if k != "id"])
            sql += "\nWHERE id=%(id)s RETURNING *"

        cr.execute(sql, store)
        res = cr.fetchone()
        cr.close()
        db.close()
    except Exception as e:
        print "*** Failded to UPSERT a store:", store, "into", host
        print "*** SQL:"
        print sql
        print "node_id =", node_id, store.get("node_id")
        print e

    return res


def get_nodes(host=None, node_id=None, is_pa=None):
    sql = """
    WITH store_counts AS (
        SELECT node_id, count(*) AS store_count FROM pa.stores GROUP BY node_id)
    SELECT n.*, COALESCE(sc.store_count, 0) AS store_count
    FROM pa.nodes AS n LEFT JOIN store_counts AS sc ON sc.node_id = n.id
    """
    if node_id is not None or is_pa is not None:
        sql += " WHERE TRUE "
        if node_id is not None:
            sql += " AND n.id = '%s'" % node_id
        if is_pa is not None:
            sql += " AND " + ("n.is_pa" if is_pa else " NOT n.is_pa")
    return dict_res(sql, host)


def get_stores(node_id=None, store_id=None, host=None):
    sql = """
    SELECT s.*, n.host_name, n.ip_addr
    FROM pa.stores AS s LEFT JOIN pa.nodes AS n
        ON n.id = s.node_id
    """
    if (node_id is not None and node_id != '*') or (store_id is not None and store_id != '*'):

        if store_id is not None and store_id != '*':
            sql += " WHERE " + ("s.id='%s'" % store_id)

        elif node_id is not None and node_id != '*':
            sql += " WHERE " + ("node_id='%s'" % node_id)
    return dict_res(sql, host)


def get_erfstores(host=None):
    sql = "SELECT * FROM erfstores"
    return dict_res(sql, host)


def get_store(store_id=None, host=None):
    res = get_stores(store_id=store_id, host=host)
    return res[0] if res else None


def dict_res(sql, host=None):
    """
    returns a list of dictionaries
    """
    db = get_db(host)
    cr = db.cursor()

    cr.execute(sql)

    rows = cr.fetchall()

    cols = [ d[0] for d in cr.description ]
    res = [dict(zip(cols, r)) for r in rows]

    cr.close()
    db.close()

    return res

#%%
def get_host_info(host):
    """
    @host - host name or IP addrss

    @returns: (hostname, ip_addr)
    """
    ip_addr = hostname = None

    try:
        socket.inet_aton(host)
        ip_addr = host
    except:
        try:
            socket.inet_pton(socket.AF_INET6, host)
            ip_addr = host
        except:
            hostname = host
            ip_addr = socket.gethostbyname(host)

    if hostname is None:
        try:
            hostname = socket.gethostbyaddr(host)
            if hostname is not None:
                hostname = hostname[0]
        except:
            pass

    return None if hostname is None and ip_addr is None else (hostname, ip_addr)

def add_pa(pa, host=None):
    pa_info = get_host_info(pa)
    if pa_info is None:
        return

    pa_ip_addr, pa_hostname = pa_info

    if host is None:
        host = source

    create_node()


#%%

def get_db(host=None):
    global source
    if host is None:
        host = source
    db = psycopg2.connect(host=host, user="postgres", database="probedb")
    db.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    return db
#%%
