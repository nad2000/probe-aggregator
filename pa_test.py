#!/usr/bin/env python

# -*- coding: utf-8 -*-

from pa import *
import random

#%%

def update_single(source):

    db = get_db(host=source)
    cr = db.cursor()
    name = sys.argv[1] if len(sys.argv) > 1 else None

    if name:
        cr.execute("""
            UPDATE pa.stores
            SET erfstore_name =%s, remote=false
            WHERE id = (SELECT id FROM pa.stores ORDER BY random() LIMIT 1)
        """, (name,))
    else:
        cr.execute("""
            UPDATE pa.stores
            SET last_ts=last_ts+1, remote=false
            WHERE id = (SELECT id FROM pa.stores ORDER BY random() LIMIT 1)
        """)

    cr.close()
    db.close()


def update(source):

    db = get_db(host=source)
    cr = db.cursor()
    while True:
        cr.execute("""
            UPDATE pa.stores
            SET last_ts=last_ts+1
            WHERE id = (SELECT id FROM pa.stores ORDER BY random() LIMIT 1)
        """)
        time.sleep(3600)

    cr.close()
    db.close()


if __name__ == "__main__":

    update_single(source=random.choice(pa_node_ip_addresses))
