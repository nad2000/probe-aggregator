#!/usr/bin/env python

# -*- coding: utf-8 -*-

from pa import *
import random

#%%
my_attr = my_id = None
def get_my_id(host=None):
    if host is None:
        host = source
    global my_id
    if my_id is None:
        my_attr = get_node_attr(host)
        my_id = my_attr["node_id"]

        
def get_random_pa_node_ip_addresses():
    return shuffled([n["ip_addr"] for n in get_pa_nodes(source)])


def propage_to_pa_cluster(message, source=None):
    """
    Probpagate chages either from a probe to the cluster
    or from a PA to the rest of PAs within the curester
    """

    table = message["table"]
    entry = message["entry"]
    event = message["event"]
    schema = message["schema"]

    # prevetn cascaded propagation:
    if "remote" in entry:
        entry["remote"] = True

    # map erfstores inot pa.stores:
    if table == "erfstores":
        schema, table = "pa", "stores"
        if event != "DELETE":
            node_id = get_node_id(source)
            # Node doen't exits in the cluster:
            if node_id is None:
                node_entry = create_node(source=source, message=entry)
                node_id = node_entry.get("node_id")
                node_msg = dict(
                    event="INSERT",
                    schema="pa",
                    table="nodes",
                    entry=node_entry)
                propage_to_pa_cluster(node_msg, source=source)

            entry["node_id"] = node_id
        entry["remote"] = False

    if event == "INSERT":
        sql = "INSERT INTO {0}.{1} ({2}) VALUES ({3})".format(
            schema, table, ', '.join(entry.keys()),
            ', '.join(["%(" + k + ")s" for k in  entry.keys()]))

    elif event == "UPDATE":
        sql = "UPDATE {0}.{1} SET {2}\n".format(schema, table,
            ', '.join([k + " = %(" + k + ")s" for k in  entry.keys()]))

    elif event == "DELETE":
        sql = "DELETE FROM {0}.{1} ".format(schema, table)

    else:
        print "## Unhandled change:", message
        return

    if event != "INSERT":
        if "id" in entry:
            sql += "WHERE id = %(id)s"
        else:
            sql += "WHERE " + ' AND '.join([k + " = %(" + k + ")s" for k in  entry.keys()
                if k in  ("erfstore_id", "probe_name", "erfstore_name", "vision") ])

    for pa in get_random_pa_node_ip_addresses():  ## list of PAs

#        if pa == source and message["table"] != "erfstores":  ## don't propagate to itself
#            continue

        try:
            db = get_db(host=pa)
            cr = db.cursor()
            cr.execute(sql, entry)
            cr.close()
            db.close()
        except Exception as e:
            print "*** Failed to execute on", pa,"SQL:"
            print sql
            print "With parameters: %s" % entry
            print e

    #print "*** Failed to propagage changes..."


def startup(source=None):
    
    for ip_addr in get_random_pa_node_ip_addresses():
        copy_full(ip_addr, source)
        copy_full(source, ip_addr)
        
    listen(source=source)


def listen(source=None):

    db = get_db(host=source)
    cr = db.cursor()
    cr.execute('LISTEN "pa.changes";')
    ##db.commit()

    while True:

        if select.select([db],[],[],600) == ([],[],[]):
            print "Timeout"
        else:
            db.poll()
            while db.notifies:
                notify = db.notifies.pop(0)
                message = json.loads(notify.payload)
                #print "Got NOTIFY:", notify
                #print "Got NOTIFY:", notify.pid, notify.channel, notify.payload
                propage_to_pa_cluster(message=message, source=source)


    cr.close()
    db.close()


if __name__ == "__main__":

    if len(sys.argv) > 1:
        source = sys.argv[1]

    startup(source=source)
