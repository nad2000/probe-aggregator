#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

import sys
import random

mode = 'cassandra' # or 'pg'
if len(sys.argv) > 1:
    mode = sys.argv[1]


if mode == 'pg':
    import pa
else:
    import cassandra_pa as pa
    pa.source = random.choice(pa.cc_nodes)

from flask_restful import Resource, Api

import select
import socket
import os
from flask import Flask, render_template, request, Response, send_from_directory, redirect, url_for
# from flask.ext.sqlalchemy import SQLAlchemy
import logging
from logging import Formatter, FileHandler
import time
import datetime
#%%

class Nodes(Resource):
    def get(self, node_id=None):
        nodes = pa.get_nodes(node_id=node_id)
        res = dict(
            meta=dict(numberOfResults=2),
            payload=[
                dict(
                    isOnline=True,
                    numberOfDatasources=n.get("store_count"),
                    probeIpAddress=n.get("ip_addr"),
                    probeName=n.get("host_name"),
                    timeLastSeen=datetime.datetime.utcnow().isoformat()
                )
                for n in nodes
            ]
        )
        return res


#from psycopg2 import connect
#import psycopg2.extensions
import json
##from forms import *

#----------------------------------------------------------------------------#
# App Config.
#----------------------------------------------------------------------------#

app = Flask(__name__)
api = Api(app)

api.add_resource(Nodes,
                 "/vision/data/pa/brief",
                 "/vision/data/pa/brief/<string:node_id>",
                 endpoint="nodes_ep")

# Automatically tear down SQLAlchemy.
'''
@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()
'''

# Login required decorator.
'''
def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            flash('You need to login first.')
            return redirect(url_for('login'))
    return wrap
'''
#----------------------------------------------------------------------------#
# Controllers.
#----------------------------------------------------------------------------#

@app.route("/nodes")
@app.route("/nodes/<ip_addr>")
def nodes(ip_addr=None):
    if ip_addr is not None:
        pa.source = ip_addr
    nodes = pa.get_nodes(host=pa.source)
    return render_template('nodes.html', nodes=nodes, source=pa.source)

@app.route('/live')
@app.route('/live/<ip_addr>')
def live(ip_addr=None):
    if ip_addr is not None:
        pa.source = ip_addr
        return render_template('live.html', source=pa.source)
    else:
        return render_template('pas.html', pas=pa.get_pas())

@app.route('/')
@app.route('/pas')
@app.route('/pas/<ip_addr>')
def pas(ip_addr=None):
    if ip_addr is not None:
        pa.source = ip_addr
    return render_template('pas.html', pas=pa.get_pas(pa.source))


@app.route('/clean')
@app.route('/clean/<ip_addr>')
def clean(ip_addr=None):
    pa.clean_data(ip_addr)
    return redirect(url_for('live'))

@app.route('/remove/<ip_addr>/<node_id>')
@app.route('/pa/<ip_addr>/remove/<node_id>')
def remove(node_id=None, ip_addr=None):
    pa.remove_node(node_id=node_id, host=ip_addr)
    time.sleep(5)

    #return redirect(url_for('eps'))
    return redirect(request.referrer)

@app.route('/stream')
@app.route('/stream/<ip_addr>')
def stream(ip_addr=None):
    """
    Some reading:

    http://www.html5rocks.com/en/tutorials/eventsource/basics/

    NB! messages should be separated with "\n\n" (mulitline message) and prefixed with "data:"
    (each line of the message)
    """
    if ip_addr is not None:
        source = ip_addr
    else:
        source = random.choice(pa.pa_node_ip_addresses)

    def gen():

        db = pa.get_db(host=source)
        cr = db.cursor()
        cr.execute('LISTEN "pa.changes.web";')

        while True:

            if select.select([db],[],[], 60) == ([],[],[]):
                yield "data: Timeout\n\n"
                #print "Timeout"
            else:
                db.poll()
                while db.notifies:
                    notify = db.notifies.pop(0)
                    message = json.loads(notify.payload)
                    #print "Got NOTIFY:", notify.pid, notify.channel
                    #print "Got NOTIFY:", notify.pid, notify.channel, notify.payload
                    #p.trigger(notify.channel, message["event"] + " of " + message["table"] , message["entry"])
                    yield "data: %s\n\n" % notify.payload
                    #yield "event: %s\ndata: %s\n\n" % (message["event"] + " of " + message["table"], notify.payload)

    return Response(gen(), mimetype="text/event-stream",
                    headers={
                        "Content-Type": "text/event-stream",
                        "Cache-Control": "no-cache",
                        "Connection": "keep-alive"
                    })

@app.route('/store/<store_id>')
@app.route('/store/<ip_addr>/<store_id>')
def store(store_id=None, ip_addr=None):
    source = ip_addr if ip_addr is not None else pa.source
    if store_id is None:
        stores = pa.get_stores(node_id=node_id, host=source)
        return render_template('stores.html', stores=stores, source=source)
    else:
        store = pa.get_store(store_id, host=source)
        return render_template('store.html', store=store, source=source)


@app.route('/stores')
@app.route('/stores/<ip_addr>')
@app.route('/stores/<ip_addr>/<node_id>')
def stores(node_id=None, ip_addr=None):
    stores = pa.get_stores(node_id=node_id, host=ip_addr)
    return render_template('stores.html', stores=stores, source=ip_addr if ip_addr else pa.source)


@app.route('/eps')
@app.route('/eps/<ip_addr>')
def eps(ip_addr=None):
    if ip_addr is None:
        ip_addr = pa.source
    else:
        pa.source = ip_addr
    probes = pa.get_eps(ip_addr)
    return render_template('eps.html', probes=probes, source=ip_addr)


@app.route('/connect/<source>/<ep_ip_addr>')
def connect_probe(ep_ip_addr, source=None):
    if source is None:
        source = pa.source
    else:
        pa.source = source
    pa.push_to_pa(ep_ip_addr)
    return  redirect("/eps/" + source)


@app.route('/pa/<ip_addr>', methods=["POST", "GET"])
def pa_setup(ip_addr):
    pa_to_join = ''
    if request.method == "POST":
        pa_to_join = request.form.get("pa_to_join")
        if pa_to_join is not None or pa_to_join.strip() != "":
            pa.create_node(pa=ip_addr, ip_addr=pa_to_join, is_pa=True)

    pas = pa.get_pas_in_cluster(host=ip_addr)

    return render_template('pa.html', pas=pas, pa_to_join=pa_to_join, source=ip_addr)



@app.route('/probe/<probe_ip_addr>', methods=["POST", "GET"])
def probe(probe_ip_addr):
    pas_to_join = ''
    if request.method == "POST":
        pas_to_join = request.form["pas_to_join"]
        if pas_to_join is not None:
            for p in pas_to_join.split():
                pa.create_node(pa=probe_ip_addr, ip_addr=p, is_pa=True)
#            for p in pas_to_join.split():
#                pa.create_node(pa=p, ip_addr=probe_ip_addr)

    pas = pa.get_pas(probe_ip_addr=probe_ip_addr)

    return render_template('probe.html',
                          probe=probe_ip_addr,
                          pas=pas,
                          pas_to_join=pas_to_join)


#
#@app.route('/login')
#def login():
#    form = LoginForm(request.form)
#    return render_template('forms/login.html', form=form)

#
#@app.route('/register')
#def register():
#    form = RegisterForm(request.form)
#    return render_template('forms/register.html', form=form)


#@app.route('/forgot')
#def forgot():
#    form = ForgotForm(request.form)
#    return render_template('forms/forgot.html', form=form)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')
# Error handlers.

#@app.errorhandler(500)
#def internal_error(error):
#    #db_session.rollback()
#    return render_template('errors/500.html'), 500
#
#
#@app.errorhandler(404)
#def not_found_error(error):
#    return render_template('errors/404.html'), 404

if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

#----------------------------------------------------------------------------#
# Launch.
#----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':

#    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#    sock.bind(('', 0))
#    port = sock.getsockname()[1]
#    sock.close()
#
#    print "Current port:", port
#    app.run(host='0.0.0.0', port=port, debug=True, threaded=(True if mode == 'pg' else False))

    app.run(host='0.0.0.0', port=4444, debug=True)

