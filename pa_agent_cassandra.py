#!/usr/bin/env python

# -*- coding: utf-8 -*-

from cassandra_pa import *
import random
from uuid import UUID

#%%
def propage_to_pa_cluster(message, source=None):
    """
    Propagates chages either from a probe to the cluster
    or from a PA to the rest of PAs within the curester
    """

    table = message["table"]
    entry = message["entry"]
    event = message["event"]
    schema = message["schema"]

    # map erfstores inot pa.stores:
    if table == "erfstores":
        schema, table = "pa", "stores"
        if event != "DELETE":
            node_id = get_node_id(source)
            if node_id is None:
                node_id = create_node(source=source, message=entry)
            entry["node_id"] = node_id

    if "id" in entry and type(entry["id"]) is not UUID:
        entry["id"] = UUID(entry["id"])

    if "node_id" in entry and type(entry["node_id"]) is not UUID:
        entry["node_id"] = UUID(entry["node_id"])

    if event == "INSERT":
        sql = "INSERT INTO {0} ({1}) VALUES ({2})".format(
            table, ', '.join(entry.keys()),
            ', '.join(["%(" + k + ")s" for k in  entry.keys() if k not in [u"version", u"remote"]]))

    elif event == "UPDATE":
        sql = "UPDATE {0} SET {1}\n".format(table,
            ', '.join([k + " = %(" + k + ")s" for k in  entry.keys() if k not in [u"version", u"remote", u"id"]]))

    elif event == "DELETE":
        sql = "DELETE FROM {0} ".format(table)

    else:
        print "## Unhandled change:", message
        return

    if event != "INSERT":
        if "id" in entry:
            sql += "WHERE id = %(id)s"
        else:
            sql += "WHERE " + ' AND '.join([k + " = %(" + k + ")s" for k in  entry.keys()
                if k in  ("erfstore_id", "probe_name", "erfstore_name", "vision") ])

    nodes = cc_nodes[:]
    random.shuffle(nodes)

    for pa in ["127.0.0.1"]: ## nodes:

        try:
            c = get_cas(pa)
            s = c.connect("pa")
            #print "### Attempting to execute:", sql
            #print "### with data:", etnry
            s.execute(sql, entry)
            s.shutdown()
            c.shutdown()
            break
        except Exception as e:
            print "*** Failed to execute on", pa, "SQL:"
            print sql
            print "With parameters: %s" % entry
            print e
            try:
                s.shutdown()
            except:
                pass
            try:
                c.shutdown()
            except:
                pass
            #print "*** Failed to propagage changes..."

def listen(source=None):

    db = get_db(host=source)
    cr = db.cursor()
    cr.execute('LISTEN "pa.changes";')
    ##db.commit()

    while True:

        if select.select([db],[],[],600) == ([],[],[]):
            print "Timeout"
        else:
            db.poll()
            while db.notifies:
                notify = db.notifies.pop(0)
                message = json.loads(notify.payload)
                #print "Got NOTIFY:", notify
                #print "Got NOTIFY:", notify.pid, notify.channel, notify.payload

                propage_to_pa_cluster(message=message, source=source)

    cr.close()
    db.close()


if __name__ == "__main__":

    if len(sys.argv) > 1:
        source = sys.argv[1]

    listen(source=source)
