#!/usr/bin/env python

# -*- coding: utf-8 -*-

import select
#%%

#from flask import Flask, Response, jsonify, request
import time
import psycopg2
import json
import psycopg2.extensions
import pusher

def get_db(host=None):
    if host is None:
        host="192.168.132.111"
    db = psycopg2.connect(host=host, user="postgres", database="probedb")
    db.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    return db

def listen():

    db = get_db()
    cr = db.cursor()
    p = pusher.Pusher(app_id='134081', key='6060792430ceb3b467ac', secret='98ce4ab9970d09e750d6', ssl=True, port=443)
    
    cr.execute('LISTEN "pa.changes";')
    #db.commit()

    while True:

        if select.select([db],[],[],600) == ([],[],[]):
            print "Timeout"
        else:
            db.poll()
            while db.notifies:
                notify = db.notifies.pop(0)
                message = json.loads(notify.payload)
                #print "Got NOTIFY:", notify.pid, notify.channel
                print "Got NOTIFY:", notify.pid, notify.channel, message
                p.trigger(notify.channel, message["event"] + " of " + message["table"] , message["entry"])


    cr.close()
    db.close()


if __name__ == "__main__":
    listen()
