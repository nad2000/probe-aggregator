# RUN FORM BASH:
# echo http://127.0.0.1:5000/?{1..1000} | xargs -n 1 -P 1000 curl -s

from flask import Flask, Response, jsonify, request
import time
import pg8000
import json

request_count = 0
app = Flask(__name__)

@app.route("/")
def hello():
    def resp():
        global request_count
        t0 = time.time()
        yield "Hello there!\n"
        yield "Previous reqest #{0}\n".format(request_count)
        time.sleep(10)
        request_count += 1
        yield "Request #{0}\n".format(request_count)
        yield "Request took {0}\n".format(time.time() - t0)
    return Response(resp(), mimetype="text/plain")


def get_db(host=None):
    if host is None:
        host="192.168.132.111"
    return pg8000.connect(host=host, user="postgres", database="probedb")

@app.route('/api/v1.0/rotfile/<store_id>', methods=['GET'])
def get_sources(store_id=None):

    db = get_db()
    cr = db.cursor()
    sql = """SELECT id::text, erfstore_id, probe_name, erfstore_name, tablespace, cluster_partition,
       vision, last_ts, next_flow_id, archive, created_by, temporary
    FROM erfstores"""
    if store_id is not None and store_id != '*':
        sql += " WHERE " ("erfsore_id='%s'" % store_id if len(store_id) > 10 else "id='%s'::uuid" % store_id)

    cr.execute(sql)
    stores = cr.fetchall()
    cols = [ d[0] for d in cr.description ]
    res = [dict(zip(cols, r)) for r in stores]

    cr.close()
    db.close()
    return jsonify({"stores": res})

@app.route('/api/v1.0/data', methods=['GET'])
def get_data():

    db = get_db()
    cr = db.cursor()
    sql = """SELECT ts::text, some_data FROM some_data2
    WHERE ts IS NOT NULL
    ORDER BY ts DESC LIMIT 10
    """

    cr.execute(sql)
    data = cr.fetchall()
    cr.close()
    db.close()
    return json.dumps(data) ## jsonify(stores)


@app.route('/api/v1.0/update', methods=['POST'])
def update_stres():

    if not request.json or not 'stores' in request.json:
        abort(400)


    db = get_db(host="192.168.132.139")
    cr = db.cursor()

    for st in request.json["stores"]:


        sql = ("INSERT INTO stores (" + ','.join(st.keys())
            + ") VALUES (" + ','.join( ["'" + str(v) + "'" for v in st.values()]) + ")" )
        try:
            cr.execute(sql)
        except:
            return jsonify({"sql": sql})

    db.commit()
    cr.close()
    db.close()
    return jsonify(dict(ok="OK"))


@app.route('/api/v1.0/insert/<some_data>', methods=['GET'])
def insert_data(some_data):
    db = get_db(host="192.168.132.139")
    cr = db.cursor()
    cr.execute("INSERT INTO some_data2(ts, some_data) VALUES(current_timestamp, '%s')" % some_data)
    db.commit()
    cr.close()
    db.close()
    return jsonify({'inserted': some_data})

if __name__ == "__main__":
    #app.run(threaded=True, debug=False)
    app.run(threaded=True, debug=True)

