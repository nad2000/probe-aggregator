# -*- coding: utf-8 -*-

import select
#%%

#from flask import Flask, Response, jsonify, request
import time
import sys
import json
import psycopg2.extensions
import psycopg2
from psycopg2 import connect
from cassandra.cluster import Cluster
from cassandra.query import dict_factory

import random
from pa import *
import uuid

cc_nodes = ["127.0.0.%i" % i for i in range(1,5)]
source = "192.168.132.111"

#%%

def shuffled_nodes():
    nodes = cc_nodes[:]
    random.shuffle(nodes)
    return nodes

def get_cas(host=None):
    if host is None:
        host = random.choice(cc_nodes)
    if host in cc_nodes:
        return Cluster([host])
    else:
        return None


#%%

def get_pas(host=None, probe_ip_addr=None):
    if probe_ip_addr is not None:
        pas = []
        for i, pa_ip_addr in enumerate(cc_nodes):

            try:
                is_connected = get_node_id(ip_addr=probe_ip_addr, host=pa_ip_addr) is not None
                is_alive = True
            except Exception as e:
                is_alive = is_connected = False

            pas.append(dict(
                ip_addr=pa_ip_addr,
                is_connected=is_connected,
                is_alive=is_alive,
                name="PA%i" % (i+1)
            ))
    else:
        pas = [dict(ip_addr=ip_addr, name="PA%i" % (i+1) ) for (i, ip_addr) in enumerate(cc_nodes)]
    return pas

#%%

def get_eps(host=None):
    nodes = get_nodes(host)
    nodes_ip_addrs = [n["ip_addr"] for n in nodes]
    nodes = dict(zip(nodes_ip_addrs, nodes))
    return [dict(
            ip_addr=ip_addr,
            connected=(ip_addr in nodes_ip_addrs),
            id=(nodes[ip_addr]["id"] if ip_addr in nodes else None)) for ip_addr in ep_ip_addresses]


def remove_node(node_id=None, host=None):
    try:
        c = get_cas(host)
        s = c.connect("pa")
        if type(node_id) is not uuid.UUID:
            node_id = uuid.UUID(node_id)
        for row in s.execute("SELECT id FROM stores WHERE node_id=%s", (node_id,)):
            s.execute("DELETE FROM stores WHERE id=%s", (row.id,))
        s.execute("DELETE FROM nodes WHERE id=%s", (node_id,))
        s.shutdown()
        c.shutdown()
    except Exception as e:
        print "*** Failed to delete the node: %s" % node_id
        print e


def get_node_id(ip_addr, host=None):

    if host is not None:
        ccs = [host]
    else:
        ccs = cc_nodes[:]
        random.shuffle(ccs)
    for host in ccs:
        try:
            c = get_cas(host)
            s = c.connect("pa")
            rows = s.execute("SELECT id FROM nodes WHERE ip_addr='%s'" % ip_addr)
            s.shutdown()
            c.shutdown()

            if rows:
                return rows[0].id
            print "*** Failed to get node ID from %s of %s" % (host, ip_addr)

        except Exception as e:
            print "*** Failed to get node ID"
            print e
            if host is not None:
                raise e

    print "*** Failed to get node ID"
    return None


def get_node(node_id):

    for pa in cc_nodes:
        try:
            c = get_cas(pa)
            s = c.connect("pa")
            s.row_factory = dict_factory
            if type(node_id) is str:
                node_id = uuid.UUID(node_id)
            rows = s.execute("SELECT * FROM nodes WHERE id=%s", (node_id,))
            s.shutdown()
            c.shutdown()

            if rows:
                return rows[0]
        except Exception as e:
            print "*** Failed to get node from %s" % pa
            print e

    print "*** Failed to get node ID"

def clean_data(host):

    c = get_cas(host)
    s = c.connect("pa")
    s.execute("DELETE FROM stores")
    s.execute("DELETE FROM nodes")
    s.shutdown()
    c.shutdown()


def create_node(source, message=None, host=None):

    if host is None:
        host = random.choice(cc_nodes)

    try:
        db = get_db(host=source)
        cr = db.cursor()
        cr.execute("SELECT * FROM pa.get_node_attr()")
        node_id, host_name, ip_addr = cr.fetchone()
        cr.close()
        db.close()

        c = get_cas(host)
        s = c.connect("pa")

        if type(node_id) is not uuid.UUID:
            node_id = uuid.UUID(node_id)

        s.execute(
        """
            INSERT INTO nodes (id, ip_addr, host_name, store_count)
            VALUES (%s, %s, %s, %s)
        """, (node_id, ip_addr, host_name, 0))
        s.shutdown()
        c.shutdown()

        return node_id

    except Exception as e:
        print "*** Failed to create a node entry at %s: (%s, %s, %s)" % (source, node_id, host_name, ip_addr)
        print e

    print "*** Failed to create node: (%s, %s, %s)" %  (node_id, host_name, ip_addr)


def get_nodes(host=None, node_id=None):
    sql = "SELECT * FROM nodes"
    if node_id is not None:
        sql += " WHERE node_id = %s" % node_id
    return dict_res(sql, host)

def get_stores(node_id=None, store_id=None, host=None):

    c = get_cas(host)
    s = c.connect("pa")

    sql = "SELECT id, host_name, ip_addr FROM nodes "
    if node_id is not None and node_id != '*':
        sql += " WHERE " + ("id=%s" % node_id)
    nodes = s.execute(sql)
    nodes = dict(zip([n.id for n in nodes], nodes))

    s.row_factory = dict_factory
    sql = "SELECT * FROM stores "
    if store_id is not None and store_id != '*':
        sql += " WHERE " + ("id=%s" % store_id)
    elif node_id is not None and node_id != '*':
        sql += " WHERE " + ("node_id=%s" % node_id)
    stores = s.execute(sql)
    s.shutdown()
    c.shutdown()

    for s in stores:
        node_id = s["node_id"]
        s["host_name"] = nodes[node_id].host_name
        s["ip_addr"] = nodes[node_id].ip_addr

    return stores


def get_store(store_id=None, host=None):
    res = get_stores(store_id=store_id, host=host)
    return res[0] if res else {}


def dict_res(sql, host=None):
    """
    returns a list of dictionaries
    """
    nodes = shuffled_nodes() if host is None else [host]
    for pa in nodes:
        try:
            c = get_cas(host)
            s = c.connect("pa")
            s.row_factory = dict_factory
            rows = s.execute(sql)
            s.shutdown()
            c.shutdown()
            return rows
        except Exception as e:
            print "Failed to execute on %s:\n%s" % (pa, sql)
            print e



def copy_full(src=None, dst=None):

    data = StringIO()

    src_db = get_db(host=src)
    src_cr = src_db.cursor()

    #dst_db = get_db(host=dst)
    #dst_db.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    #dst_cr = dst_db.cursor()

    schema = "pa"
    for table in ["nodes", "stores"]:

        src_cr.execute("""
            SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = %s
                AND table_name = %s
        """, (schema, table))
        cols = [c[0] for c in src_cr.fetchall() if c[0] not in ["version", "remote"]]

        f = open("/home/nad2000/" + table + ".csv", "w")
        src_cr.copy_to(f, schema + '.' + table, columns=cols)
        #dst_cr.copy_from(data, table=schema + '.' + stage, columns=cols)
        f.close()
        continue



        #stage = "{0}_{1}".format(table, random.randint(1,1000000))
        stage = table + "_"
        data.seek(0)

        data.seek(0)

        try:
            c = get_cas(host)
            s = c.connect("pa")
            s.execute(
                "COPY {0}.{1} FROM STDIN WITH DELIMITER='\\t'".format(schema, table),


            s.shutdown()
            c.shutdown()
        except Exception as e:
            print "Failed to execute on %s:\n%s" % (pa, sql)
            print e


    src_cr.close()
    src_db.close()

